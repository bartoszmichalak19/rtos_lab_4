# Project Title
Field of study: Advanced Applied Electronics <br >
Course: Real Time Operating Systems <br >
Laboratory list no.4.  <br >
author: Bartosz Michalak <br>

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [Tasks description](#tasks)

## About <a name = "about"></a>

This project is created to implement multithread applications for RTOS
laboratory classes based on Xenomai. Apps are written in C using Xenomai API. 

## Getting Started <a name = "getting_started"></a>

Short introduction just to run code related to tasks. They are divided into two directories, every one has a similar structure. There is Makefile prepared to build task-related app. 

### Prerequisites

Apps were written and tested on prepared Virtual Machine by the lecturer based on ubuntu with xenomai installed and configured.

### Building and Running apps <a name = "usage"></a>

Example of usage for one of the tasks.


```sh
cd task3
make
./task3
```

## Tasks descriptions <a name = "tasks"></a>

**Task1** <br > 

The task was to compute PI number using monte carlo method whit mulitask computations. 
There is a while loop that waits until all threads calculate sum of pi values, then mean_pi value is obtained and printed out.
```c
void pi_calculation_task(void *arg) {

  for (unsigned int i = 0; i < points_quantity_; i++) {
    int x = rand() / (float)RAND_MAX;
    int y = rand() / (float)RAND_MAX;
    if (x * x + y * y <= 1)
      points_inside_circle_++;
  }
  float monte_carlo_result = monte_carlo_result + 4.0 * points_inside_circle_ / points_quantity_;
  printf("sum = %f ", monte_carlo_result);
  points_inside_circle_ = 0;
  threads_counter++;
}
```

```c
 for (size_t i = 0; i < THREADS_QTY; i++) {
    rt_task_create(&tasks_arr[i], NULL, 0, 10, 0);
    rt_task_start(&tasks_arr[i], pi_calculation_task, NULL);
  }

  while (threads_counter < THREADS_QTY) {
    rt_task_sleep(rand() % 100 + 10);
  }

  float mean_pi = monte_carlo_result / THREADS_QTY;
  printf("MEAN PI = %f ", mean_pi);

```