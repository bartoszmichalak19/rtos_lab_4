#include <alchemy/task.h>
#include <alchemy/timer.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>


#define THREADS_QTY 16

RT_TASK tasks_arr[THREADS_QTY];

const unsigned int points_quantity_ = 10000;
int points_inside_circle_ = 0;
float monte_carlo_result = 0.0f;
unsigned threads_counter = 0;

void pi_calculation_task(void *arg) {

  for (unsigned int i = 0; i < points_quantity_; i++) {
    int x = rand() / (float)RAND_MAX;
    int y = rand() / (float)RAND_MAX;
    if (x * x + y * y <= 1)
      points_inside_circle_++;
  }
  float monte_carlo_result = monte_carlo_result + 4.0 * points_inside_circle_ / points_quantity_;
  printf("sum = %f ", monte_carlo_result);
  points_inside_circle_ = 0;
  threads_counter++;
}

int main(int argc, char **argv) {

  // Lock the memory to avoid memory swapping for this program
  mlockall(MCL_CURRENT | MCL_FUTURE);

  printf("Start tasks\n");

  for (size_t i = 0; i < THREADS_QTY; i++) {
    rt_task_create(&tasks_arr[i], NULL, 0, 10, 0);
    rt_task_start(&tasks_arr[i], pi_calculation_task, NULL);
  }

  while (threads_counter < THREADS_QTY) {
    rt_task_sleep(rand() % 100 + 10);
  }

  float mean_pi = monte_carlo_result / THREADS_QTY;
  printf("MEAN PI = %f ", mean_pi);
  // Wait for Ctrl-C
  pause();

  return 0;
}
